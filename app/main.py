"""
Описание модулей
telebot - API tg
sqlite3 - БД
loguru - логирование
uuid - генерит случайный id пользователю
config - токен бота
"""
import sqlite3
import threading
import time
from datetime import datetime
from uuid import uuid4

import schedule
import telebot
from loguru import logger

from config import TOKEN, USER_ID, CHAT_ID

# Создание экземпляра бота
bot = telebot.TeleBot(TOKEN)

# Соединение с базой данных
conn = sqlite3.connect("/dataBase/wishlists.db", check_same_thread=False)
cursor = conn.cursor()
# Создание таблицы для хранения списков желаемых подарков
cursor.execute("""
    CREATE TABLE IF NOT EXISTS users (
        user_id TEXT,
        id TEXT
    )
""")
cursor.execute("""
    CREATE TABLE IF NOT EXISTS wishlists (
        id INTEGER PRIMARY KEY,
        user_id TEXT,
        gift TEXT,
        booking INT,
        username TEXT
    )
""")
conn.commit()

keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
keyboard.add('Показать мой 📃', 'Добавить желание🤌', 'Поделиться ссылкой🛫', '/start')

keyboard_exit = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
keyboard_exit.add('Назад 🔙')


@bot.message_handler(commands=['start'])
def start_message(message):
    """Приветственное сообщение и инициализация пользователя в БД
    """
    try:
        bot.reply_to(message, """
        Пользуйтесь кнопочками для взаимодействия с Ботом!
        
        """, reply_markup=keyboard)
        chat_id = message.chat.id
        wishlist_id = int(uuid4())
        user_id = message.from_user.id

        cursor.execute(f"SELECT * FROM users WHERE user_id='{user_id}'")
        check_exist = cursor.fetchone()
        if check_exist is None:
            cursor.execute(f"INSERT INTO users (id, user_id) "
                           f"VALUES ('{wishlist_id}', '{user_id}')")
            conn.commit()
            bot.send_message(chat_id,
                             "Вы зарегистрированы в боте. Взаимодействуйте с ботом через кнопки")
            logger.info(f"Инициирован пользователь '{user_id}'. Присвоенный id: {wishlist_id}")

        if " " in message.text:
            wishlist_id = message.text.split("/start ")[1]
            cursor.execute(f"SELECT user_id FROM users WHERE id='{wishlist_id}'")
            user_id = cursor.fetchone()[0]
            # logger.info(user_id)
            bot.send_message(chat_id, "У Вашего друга следующий список желаний: ")
            get_wishlist(message, chat_id, user_id, wishlist_id)
    except Exception as error:
        logger.debug(f"Произошла ошибка:{error}")


@bot.message_handler(commands=['backup'])
def command_backup(message):
    # Проверка на администратора группы
    user_id = message.from_user.id
    chat_id = message.chat.id
    if not is_group_admin(chat_id, user_id):
        bot.reply_to(message, "Извините, вы не администратор группы.")
        return

    # Получение текущей даты
    current_date = datetime.now().strftime('%Y-%m-%d')

    backup_filename = f'bot_gift_backup_{current_date}.db'

    with open(backup_filename, 'wb') as backup_file:
        with open('/dataBase/wishlists.db', 'rb') as original_db:
            backup_file.write(original_db.read())

    with open(backup_filename, 'rb') as backup_file:
        bot.send_document(CHAT_ID, backup_file)
        bot.reply_to(message, "Резервная копия базы данных отправлена в группу.")


@bot.message_handler(func=lambda message: True)
def handle_message(message):
    """Обработчик текстовых сообщений
    """
    try:
        chat_id = message.chat.id
        user_id = message.from_user.id

        cursor.execute(f"SELECT id FROM users WHERE user_id='{user_id}'")
        wishlist_id = cursor.fetchone()
        if wishlist_id is None:
            bot.send_message(chat_id, "Перезагрузите бота. /start")
            logger.debug("Пользователь не инициирован в таблице users")

        if message.text.startswith("Показать мой 📃"):
            get_wishlist(message, chat_id, user_id)
        elif message.text.startswith("Добавить желание🤌"):
            edit_wishlist(message, chat_id, user_id)
        # elif message.text.startswith("/edit"):
        #     gift_delete_edit(message)
        elif message.text.startswith("Поделиться ссылкой🛫"):
            share_link(user_id)
        else:
            bot.send_message(chat_id, "чего? попробуй сделать очередную итерацию\n"
                                      "В любой неожиданной ситуации жми /start", reply_markup=keyboard)

    except Exception as error:
        logger.debug(f"Произошла ошибка:{error}")


def share_link(user_id):
    """Ссылка с параметром. При переходе по ней запускается /start с параметров,
    который указывает на wishlist
    формат: https://t.me/<юзернейм_бота>?start=test
    """
    try:
        cursor.execute(f"SELECT * FROM users WHERE user_id='{user_id}'")
        # link = f"tg://resolve?domain={bot.get_me().username}&start=/start%{cursor.fetchone()[1]}"
        # logger.info(link)
        # bot.send_message(user_id, link)
        link = f"https://t.me/{bot.get_me().username}?start={cursor.fetchone()[1]}"

        bot.send_message(user_id, link, reply_markup=keyboard)
    except Exception as error:
        logger.debug(f"Произошла ошибка:{error}")


def get_username(message):
    """Получение Username пользователя.
    Если username не установлен, то id
    """
    usr = bot.get_chat_member(message.chat.id, message.from_user.id)
    if not usr.user.username:
        return usr.user.id
    return usr.user.username


def get_wishlist(message, chat_id, user_id, wishlist_id=0):
    """Отправка в чат всего списка желаний пользователя
    """
    try:
        cursor.execute(f"SELECT * FROM wishlists WHERE user_id='{user_id}'")
        row = cursor.fetchall()  # строка из результата запроса
        # logger.info(row)
        if not row:
            bot.send_message(chat_id, "🫥 - пользователь еще не добавил подарки в список желаний \n")
        else:
            bot.send_message(chat_id, "🔐 - подарок дарит кто-то другой\n"
                                      "🔓 - подарок можно забронировать")
        count = 0
        for gift in row:
            keyboard_edit = telebot.types.InlineKeyboardMarkup()
            # logger.info(gift)
            count += 1
            if wishlist_id and gift[3] == chat_id:
                keyboard_edit.add(telebot.types.InlineKeyboardButton(text="Не буду дарить! 👊",
                                                                     callback_data=f"/b?"
                                                                                   f"g_i={gift[0]}"
                                                                                   f"&u_i={user_id}"
                                                                                   f"&c_i={chat_id}"
                                                                                   f"&i_b=1"
                                                                                   f"&u_n=None"))
            elif wishlist_id and gift[3] != chat_id and gift[3] != 0:
                cursor.execute(f"SELECT username FROM wishlists WHERE id='{gift[0]}'")
                username = cursor.fetchone()[0]
                keyboard_edit.add(telebot.types.InlineKeyboardButton(text=f"🔐 уже дарит - "
                                                                          f"@{username}",
                                                                     callback_data=f"/b?"
                                                                                   f"g_i={gift[0]}"
                                                                                   f"&u_i={user_id}"
                                                                                   f"&c_i={chat_id}"
                                                                                   f"&i_b=0"
                                                                                   f"&u_n=None"))
            elif wishlist_id:
                username = get_username(message)
                # logger.info(username)
                keyboard_edit.add(telebot.types.InlineKeyboardButton(text="Я подарю! ✍️",
                                                                     callback_data=f"/b?"
                                                                                   f"g_i={gift[0]}"
                                                                                   f"&u_i={user_id}"
                                                                                   f"&c_i={chat_id}"
                                                                                   f"&i_b={2}"
                                                                                   f"&u_n="
                                                                                   f"{username}"))
            else:
                # logger.info(user_id)
                keyboard_edit.add(telebot.types.InlineKeyboardButton(text="ред. ✍️",
                                                                     callback_data=f"/ed?"
                                                                                   f"g_i={gift[0]}"
                                                                                   f"&u_i={user_id}"
                                                                                   f"&c_i={chat_id}"
                                                                                   f"&i_b=0"
                                                                                   f"&u_n=None"))
            if gift[3]:
                cursor.execute(f"SELECT username FROM wishlists WHERE id='{gift[0]}'")
                username = cursor.fetchone()[0]
                # logger.info(username)
                booking_smail = f"🔐 {gift[2]}\nуже дарит - @{username}"
            else:
                booking_smail = f"🔓 {gift[2]}"
            # logger.info(chat_id)
            # logger.info(gift)
            bot.send_message(chat_id, f"{count}. {booking_smail}",
                             reply_markup=keyboard_edit)
    except Exception as error:
        logger.debug(f"Произошла ошибка:{error}")


def edit_wishlist(message, chat_id, user_id):
    """Сообщение с просьбой ввести список подарков. Вызов ф-ии add_gift(...)
    """
    bot.send_message(chat_id, 'Введите список подарков через запятую\n'
                              'или по одному названию:\n', reply_markup=keyboard_exit)
    bot.register_next_step_handler(message, add_gift, user_id)


def add_gift(message, user_id):
    """Добавление желаний в список
    """
    try:
        if message.text == "Назад 🔙":
            bot.send_message(user_id, "Вы вернулись в главное меню. Выберите действие!", reply_markup=keyboard)

        new_gift = message.text
        new_gift = new_gift.split(',')
        bad_gift = ('Показать мой 📃', 'Добавить желание🤌', 'Поделиться ссылкой🛫', '/start', 'Назад 🔙')
        for gift in new_gift:
            if len(gift) > 4000:
                bot.send_message(user_id, "Длина названия должна быть от 1, до 4096 символов\n"
                                          "Лев Николаевич, Вы действительно написали Войну и мир?\nДа, написал.\n"
                                          "А Анну Каренину?\nДа, и её тоже.\nВоскресение?\nДа, и Воскресение тоже.\n"
                                          "Ну, а почему вы больше ничего не написали?\n А что еще можно написать, "
                                          "если уже написал Войну и мир, Анну Каренину и Воскресение?\n")
                logger.info("Длина названия должна быть от 1, до 4096 символов")
                bot.send_message(user_id, "Попробуйте еще раз\n"
                                          "Выберите следующее действие", reply_markup=keyboard)
            elif len(gift) == 0:
                logger.info("Пустое сообщение")
                bot.send_message(user_id, "Слишком короткое название.\nПопробуйте еще раз\n"
                                          "Выберите следующее действие", reply_markup=keyboard)

            elif gift in bad_gift:
                if gift != 'Назад 🔙':
                    bot.send_message(user_id, "Название не может быть командой")
                # logger.info("Название не может быть командой")
            elif gift.isspace():
                bot.send_message(user_id, "Название не может состоять из пробелов")
            else:
                cursor.execute(
                    f"INSERT INTO wishlists (user_id, gift, booking) "
                    f"VALUES ('{user_id}', '{gift.strip()}', {0})")
                conn.commit()
                bot.reply_to(message, 'Подарок добавлен в список!')
                bot.send_message(user_id, "Выберите следующее действие", reply_markup=keyboard)
                logger.info(f"Пользователь '{user_id}' добавил '{gift}"
                            f"' в список пользователя: '{user_id}'")
    except Exception as error:
        logger.debug(f"Произошла ошибка:{error}")


def gift_delete_edit(id_gift, user_id, chat_id):
    """Вызов /edit{N}, где N - id_gift в таблице wishlists
    отображение кнопок удалить и редактировать
    """
    try:
        cursor.execute(f"SELECT * FROM wishlists WHERE id='{id_gift}'")
        row = cursor.fetchone()  # строка из результата запроса

        if row is None:
            logger.debug("Обращение к несуществующему в БД желанию!")
        elif user_id != row[1]:
            logger.debug("Обращение к чужому в БД желанию!")
        else:
            keyboard_delete_edit = telebot.types.InlineKeyboardMarkup()
            keyboard_delete_edit.add(telebot.types.InlineKeyboardButton(
                text="удалить",
                callback_data=f"delete-{id_gift}&u_i={user_id}&c_i={chat_id}"))
            keyboard_delete_edit.add(telebot.types.InlineKeyboardButton(
                text="редактировать",
                callback_data=f"edit-{id_gift}&u_i={user_id}&c_i={chat_id}"))
            bot.send_message(chat_id, f"Подарок: {row[2]}", reply_markup=keyboard_delete_edit)
    except Exception as error:
        logger.debug(f"Произошла ошибка:{error}")


@bot.callback_query_handler(func=lambda call: True)
def handle_callback(call):
    """Обработка нажатия кнопок удалить/редактировать
    Парс следующих строк:
    /edit?gift_id=17&user_id=339633650&chat_id=339633650
    /ed?g_i=18&u_i=339633650&c_i=339633650&i_b=0

    /booking?gift_id=13&user_id=1066396136
    /b?g_i=17&u_i=339633650&c_i=1066396136&i_b=0
    """
    try:
        bot.edit_message_reply_markup(chat_id=call.message.chat.id,
                                      message_id=call.message.message_id,
                                      reply_markup=None)
        # logger.info(call.data)
        if call.data.startswith("/ed"):
            string = call.data

            gift_id = string.split("/ed?g_i=")[1].split("&")[0]
            user_id = string.split("&u_i=")[1].split("&")[0]
            chat_id = int(string.split("&c_i=")[1].split("&")[0])
            # logger.info(gift_id)
            # logger.info(user_id)
            # logger.info(chat_id)

            gift_delete_edit(gift_id, user_id, chat_id)
        if call.data.startswith("delete-"):
            logger.info(call.data)

            gift_id = call.data.split("delete-")[1].split("&u_i")[0]
            user_id = call.data.split("&u_i=")[1].split("&")[0]
            chat_id = call.data.split("&c_i=")[1].split("&")[0]
            logger.info(gift_id)
            logger.info(user_id)
            logger.info(chat_id)
            cursor.execute(f"DELETE FROM wishlists WHERE id={gift_id}")
            conn.commit()
            bot.send_message(call.message.chat.id, "Желание удалено\n"
                                                   "Актуальный список подарков:", reply_markup=keyboard)

            get_wishlist(call.message, chat_id, user_id)
        if call.data.startswith("edit-"):
            user_id = call.data.split("&u_i=")[1].split("&")[0]
            chat_id = call.data.split("&c_i=")[1].split("&")[0]
            gift_id = call.data.split("edit-")[1].split("&u_i")[0]
            bot.send_message(call.message.chat.id, "введите новое название:\n", reply_markup=keyboard_exit)
            bot.register_next_step_handler(call.message, edit_gift, gift_id, user_id, chat_id)

        if call.data.startswith("/b"):
            string = call.data
            gift_id = string.split("g_i=")[1].split("&")[0]
            user_id = int(string.split("&u_i=")[1].split("&")[0])
            chat_id = int(string.split("&c_i=")[1].split("&")[0])
            i_booking = int(string.split("&i_b=")[1].split("&")[0])
            username = string.split("&u_n=")[1].split("&")[0]

            if i_booking > 1:  # буду дарить
                cursor.execute(f"UPDATE wishlists SET booking={chat_id},"
                               f"username='{username}' WHERE id='{gift_id}'")
            elif i_booking:  # не буду дарить
                cursor.execute(f"UPDATE wishlists SET booking=0, "
                               f"username='Null'  WHERE id='{gift_id}'")
            else:  # уже дарят
                bot.answer_callback_query(callback_query_id=call.id,
                                          text='"Этот подарок дарит другой человек!')

            conn.commit()
            bot.send_message(chat_id, "Готово! \n")

        return
    except Exception as error:
        logger.debug(f"Произошла ошибка:{error}")


def edit_gift(message, gift_id, user_id, chat_id):
    """Запрос в БД - обновить значение поля gift таблицы wishlists
    """
    try:
        if message.text == "Назад 🔙":
            bot.send_message(user_id, "Вы вернулись в главное меню. Выберите действие!", reply_markup=keyboard)
        else:
            new_gift = message.text
            cursor.execute(f"UPDATE wishlists SET gift='{new_gift}' WHERE id='{gift_id}'")
            conn.commit()
            bot.send_message(message.chat.id, "желание отредактировано\n"
                                              "Актуальный список подарков:", reply_markup=keyboard)
            get_wishlist(message, chat_id, user_id)
    except Exception as error:
        logger.debug(f"Произошла ошибка:{error}")


def daily_backup():
    current_date = datetime.now().strftime('%Y-%m-%d')

    backup_filename = f'bot_gift_backup_{current_date}.db'

    with open(backup_filename, 'wb') as backup_file:
        with open('/dataBase/wishlists.db', 'rb') as original_db:
            backup_file.write(original_db.read())

    with open(backup_filename, 'rb') as backup_file:
        bot.send_document(CHAT_ID, backup_file)


def is_group_admin(chat_id, user_id):
    if user_id in USER_ID:
        return True
    else:
        return False


def bot_polling_thread():
    bot.polling(none_stop=True, timeout=60)


# Основная функция
def main():
    # Запуск потока для bot.polling()
    bot_polling = threading.Thread(target=bot_polling_thread)
    bot_polling.start()

    # Расписание для выполнения ежедневной резервной копии каждый день в определенное время
    schedule.every().day.at("03:31").do(daily_backup)

    while True:
        schedule.run_pending()
        time.sleep(60)  # Подождите 60 секунд между итерациями цикла


if __name__ == "__main__":
    main()
