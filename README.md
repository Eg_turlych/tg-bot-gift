# Бот для создания списка подарков

# Перед запуском

Добавить свой TOKEN в app/config.py

```bash
 echo 'TOKEN = "***"' > app/config.py
 echo 'CHAT_ID = [***]' >> app/config.py
 echo 'USER_ID = [***]' >> app/config.py
```

Обновить данные в файле source/conf.py

# Запуск в Docker контейнере с пробросом папки БД

```bash
docker build -t turlych-bot .
docker run -v ./dataBase:/dataBase -d --restart=always --name turlych-bot-container turlych-bot
```

Ключ -d указывает на запуск контейнера в фоновом режиме,
а ключ --restart=always указывает на автоматический перезапуск контейнера в случае его остановки.

Остановка, удаление, билд и запуск:

```bash
docker stop turlych-bot-container
docker rm turlych-bot-container 
docker build -t turlych-bot .
docker run -v ./dataBase:/dataBase --restart=always --name turlych-bot-container turlych-bot
```
